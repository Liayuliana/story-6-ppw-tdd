import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import TestCase, Client
from django.urls import reverse, resolve
from aboutme.views import aboutme
from django.contrib.auth import authenticate, login


# Create your tests here.

class AboutmeUnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.aboutme = reverse('about-creator')

    #Test Urls
    def test_aboutme_url_is_exist(self):
        url = self.client.get('/about-creator')
        self.assertEquals(url.status_code, 200)

    #Test templates
    def test_aboutme_render_templates(self):
        response = self.client.get(self.aboutme)
        self.assertTemplateUsed(response, 'aboutme/index.html')

    #Test Views
    def test_aboutme_using_aboutme_func(self):
        found = self.aboutme
        self.assertEquals(resolve(found).func, aboutme)

    def test_aboutme_login(self):
        login = self.client.post('/login', follow=True, data={
            'username': 'george',
            'password': 'georgeuser123',
        })
        self.assertTrue(login)

    def test_aboutme_authenticated(self):
        self.client.post('/login', follow=True, data={
            'username': 'george',
            'password': 'georgeuser123',
        })
        response = self.client.get('/about-creator')
        self.assertEquals(response.status_code, 200)

    def test_aboutme_not_authenticated(self):
        self.client.post('/login', follow=True, data={
            'username': 'test',
            'password': 'testuser123',
        })
        response = self.client.get('/about-creator')
        self.assertTemplateUsed(response, 'aboutme/index.html')

class AboutmeFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(AboutmeFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(AboutmeFunctionalTest, self).tearDown()

    # Test Perubahan Tema
    def test_changingTheme(self):
        selenium = self.selenium

        # Membuka link localhost yang ingin ditest
        selenium.get('http://127.0.0.1:8000/about-creator')
        
        # Klik button Change Theme 1 kali 
        theme_button = selenium.find_element_by_class_name('gantiTema')
        theme_button.click()

        theme2_navbar =  selenium.find_element_by_class_name('navbar').value_of_css_property('background-color')
        self.assertEquals(theme2_navbar, 'rgba(53, 142, 116, 1)')

        theme2_button =  selenium.find_element_by_class_name('gantiTema').value_of_css_property('background-color')
        self.assertEquals(theme2_button, 'rgba(53, 142, 116, 1)')

        theme2_accordion_head =  selenium.find_element_by_class_name('accordion-head').value_of_css_property('background-color')
        self.assertEquals(theme2_accordion_head, 'rgba(53, 142, 116, 1)')

        theme2_copyRight =  selenium.find_element_by_class_name('copy-right').value_of_css_property('background-color')
        self.assertEquals(theme2_copyRight, 'rgba(53, 142, 116, 1)')

        theme2_identity =  selenium.find_element_by_class_name('identity').value_of_css_property('color')
        self.assertEquals(theme2_identity, 'rgba(53, 142, 116, 1)')

        theme2_page2 =  selenium.find_element_by_class_name('page-2').value_of_css_property('background-color')
        self.assertEquals(theme2_page2, 'rgba(235, 240, 239, 0.79)')

        # Klik lagi button Change Theme 
        theme_button = selenium.find_element_by_class_name('gantiTema')
        theme_button.click()

        theme1_navbar =  selenium.find_element_by_class_name('navbar').value_of_css_property('background-color')
        self.assertEquals(theme1_navbar, 'rgba(161, 73, 121, 1)')

        theme1_button =  selenium.find_element_by_class_name('gantiTema').value_of_css_property('background-color')
        self.assertEquals(theme1_button, 'rgba(161, 73, 121, 1)')

        theme1_accordion_head =  selenium.find_element_by_class_name('accordion-head').value_of_css_property('background-color')
        self.assertEquals(theme1_accordion_head, 'rgba(161, 73, 121, 1)')

        theme1_copyRight =  selenium.find_element_by_class_name('copy-right').value_of_css_property('background-color')
        self.assertEquals(theme1_copyRight, 'rgba(161, 73, 121, 1)')

        theme1_identity =  selenium.find_element_by_class_name('identity').value_of_css_property('color')
        self.assertEquals(theme1_identity, 'rgba(161, 73, 121, 1)')

        theme1_page2 =  selenium.find_element_by_class_name('page-2').value_of_css_property('background-color')
        self.assertEquals(theme1_page2, 'rgba(242, 234, 234, 1)')

        time.sleep(5)

    # Test Perilaku Accordion
    def test_accordion(self):
        selenium = self.selenium

        # Membuka link localhost yang ingin ditest
        selenium.get('http://127.0.0.1:8000/about-creator')
        
        # Accorion 1 diklik
        accordion1_head = selenium.find_element_by_class_name('accordion-head1')
        accordion1_head.click()

        accodion1_body =  selenium.find_element_by_class_name('accordion-body1').value_of_css_property('display')
        self.assertEquals(accodion1_body, 'block')

        # Accorion 2 diklik
        accordion2_head = selenium.find_element_by_class_name('accordion-head2')
        accordion2_head.click()

        accodion2_body =  selenium.find_element_by_class_name('accordion-body2').value_of_css_property('display')
        self.assertEquals(accodion2_body, 'block')

        accodion1_body =  selenium.find_element_by_class_name('accordion-body1').value_of_css_property('display')
        self.assertEquals(accodion1_body, 'none')

        # Accordion 3 diklik
        accordion3_head = selenium.find_element_by_class_name('accordion-head3')
        accordion3_head.click()

        accodion3_body =  selenium.find_element_by_class_name('accordion-body3').value_of_css_property('display')
        self.assertEquals(accodion3_body, 'block')

        accodion2_body =  selenium.find_element_by_class_name('accordion-body2').value_of_css_property('display')
        self.assertEquals(accodion2_body, 'none')

        # Accordion 3 diklik lagi
        accordion3_head = selenium.find_element_by_class_name('accordion-head3')
        accordion3_head.click()

        accodion3_body =  selenium.find_element_by_class_name('accordion-body3').value_of_css_property('display')
        self.assertEquals(accodion3_body, 'none')

        time.sleep(10)

       
