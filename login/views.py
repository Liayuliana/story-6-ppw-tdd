import datetime
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout

def welcome(request):
    if request.user.is_authenticated:
        return render(request, 'login/index_user.html')
    else:
        return redirect(login)
        
def login(request):
    if request.user.is_authenticated:
        return redirect(welcome)

    else:
        if request.method == "POST":
            username_login = request.POST.get('username')
            password_login = request.POST.get('password')
            user = authenticate(request, username=username_login, password=password_login)

            # Login berhasil
            if user is not None:
                auth_login(request, user)
                request.session['login_timestamp'] = str(datetime.datetime.now().strftime("%d %B %Y | %I:%M %p"))
                return redirect(welcome)
            # Login tidak berhasil
            else:
                return redirect(login)
                
    return render(request, 'login/login.html')

def logout(request):
    auth_logout(request)
    return redirect(login)
    