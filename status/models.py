import datetime
from django.db import models

# Create your models here.
class addStatus(models.Model):
    status_field = models.CharField(max_length = 300)
    submitted = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return ("{}".format(self.status_field))
