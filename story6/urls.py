"""story6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from aboutme import views as aboutmeviews
from books import views as booksviews
from login import views as loginviews

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('status.urls', namespace='status')),
    path('about-creator', aboutmeviews.aboutme, name='about-creator'),
    path('search-book', booksviews.books, name='books'),
    path('login', loginviews.login, name='login'),
    path('welcome', loginviews.welcome, name='welcome'),
    path('logout', loginviews.logout, name='logout'),
]
