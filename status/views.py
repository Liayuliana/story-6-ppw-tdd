from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import statusForms
from .models import addStatus
from django.contrib.auth import authenticate

# Create your views here.
def status(request):
    status_form = statusForms(request.POST or None)
    status_response = addStatus.objects.all().order_by('-submitted')

    if request.method == 'POST':
        if status_form.is_valid():
            status_form.save()
            return redirect('status:status')

    context = {
        'page_title' : "Hello, How Are You?",
        'status_form' : status_form,
        'status_response' : status_response
    }  

    if request.user.is_authenticated:
        return render(request, 'status/index_user.html', context)

    return render(request, 'status/index.html', context)



