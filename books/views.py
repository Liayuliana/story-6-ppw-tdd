from django.shortcuts import render
from django.contrib.auth import authenticate
# Create your views here.
def books(request):
    if request.user.is_authenticated:
        return render(request, 'books/index_user.html')
    else:
        return render(request, 'books/index.html')