from django.urls import path
from . import views

app_name = 'login'

urlpatterns = [
    path('login', views.login, name='login'),
    path('welcome', views.welcome, name='welcome'),
    path('logout', views.logout, name='logout'),
]

