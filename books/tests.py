import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import TestCase, Client
from django.urls import reverse, resolve
from books.views import books
from django.contrib.auth import authenticate, login

class BooksUnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.books = reverse('books')

    # Test Urls
    def test_books_urls_is_exist(self):
        url = self.client.get('/search-book')
        self.assertEquals(url.status_code, 200)

    # Test Templates
    def test_books_render_templates(self):
        response = self.client.get(self.books)
        self.assertTemplateUsed(response, 'books/index.html')

    # Test Views
    def test_books_using_books_func(self):
        found = self.books
        self.assertEquals(resolve(found).func, books)

    def test_books_login(self):
        login = self.client.post('/login', follow=True, data={
            'username': 'george',
            'password': 'georgeuser123',
        })
        self.assertTrue(login)

    def test_books_authenticated(self):
        self.client.post('/login', follow=True, data={
            'username': 'george',
            'password': 'georgeuser123',
        })
        response = self.client.get('/search-book')
        self.assertEquals(response.status_code, 200) 

    def test_book_not_authenticated(self):
        self.client.post('/login', follow=True, data={
            'username': 'test',
            'password': 'testuser123',
        })
        response = self.client.get('/search-book')
        self.assertTemplateUsed(response, 'books/index.html')
        
class BooksFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(BooksFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(BooksFunctionalTest, self).tearDown()

    def test_findBook(self):
        selenium = self.selenium

        # Membuka link localhost yang ingin ditest
        selenium.get('http://127.0.0.1:8000/search-book')
        
        # Pilih elemen yang akan diberikan input
        title_field = selenium.find_element_by_id('title-field')
        title_field.send_keys('99 cahaya') 

        # Klik tombol search
        search_button = selenium.find_element_by_class_name('submit')
        search_button.click()

        # Memunculkan table head
        table =  selenium.find_element_by_class_name('page-3').value_of_css_property('display')
        self.assertEquals(table, 'block')

        # Cari informasi buku berdasarkan input
        search_button.send_keys(Keys.RETURN)

        time.sleep(10)