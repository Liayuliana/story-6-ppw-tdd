from django.urls import path
from . import views

app_name = 'books'

urlpatterns = [
    path('search-book', views.books)
]