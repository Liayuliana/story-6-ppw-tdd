from django.shortcuts import render
from django.contrib.auth import authenticate

# Create your views here.
def aboutme(request):
    if request.user.is_authenticated:
        return render(request, 'aboutme/index_user.html')
    return render(request, 'aboutme/index.html')