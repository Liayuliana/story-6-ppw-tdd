import datetime
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import TestCase, Client
from django.urls import reverse, resolve
from status.models import addStatus
from status.forms import statusForms
from status.views import status
from django.contrib.auth import authenticate, login


# Create your tests here.

class StatusUnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.status = reverse('status:status')

    #Test urls
    def test_status_url_is_exist(self):
        response =  self.client.get('')
        self.assertEquals(response.status_code, 200)
    
    #Test templates
    def test_status_render_templates(self):
        response = self.client.get(self.status)
        self.assertTemplateUsed(response, 'status/index.html')

    #Test models
    def test_status_success_make_models_and_matches(self):
        data = addStatus.objects.create(
            status_field ='Very well',
            submitted =  datetime.datetime.now(),
        )
        counting_all_data = addStatus.objects.all().count()
        self.assertEquals(counting_all_data, 1)
        self.assertTrue(isinstance(data, addStatus))
        self.assertEquals(data.__str__(), data.status_field)
        
    #Test views 
    def test_status_using_status_func(self):
        found = self.status 
        self.assertEqual(resolve(found).func, status)

    def test_status_submit_successfully(self):
        response = self.client.post(self.status, {'status_field':'Very well'})
        self.assertEquals(response.status_code, 302) 

    def test_status_login(self):
        login = self.client.post('/login', follow=True, data={
            'username': 'george',
            'password': 'georgeuser123',
        })
        self.assertTrue(login)

    def test_status_authenticated(self):
        self.client.post('/login', follow=True, data={
            'username': 'george',
            'password': 'georgeuser123',
        })
        response = self.client.get('')
        self.assertEquals(response.status_code, 200) 

    def test_status_not_authenticated(self):
        self.client.post('/login', follow=True, data={
            'username': 'test',
            'password': 'testuser123',
        })
        response = self.client.get('')
        self.assertTemplateUsed(response, 'status/index.html')

class StatusFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(StatusFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(StatusFunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium

        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000')

        # Find the form element
        status_field = selenium.find_element_by_name('status_field')
        submit = selenium.find_element_by_class_name('btn')

        # Fill the form with data
        status_field.send_keys('kuliah rapat kuliah rapat')

        # Submitting the form
        submit.send_keys(Keys.RETURN)

        time.sleep(3)



  
        


    


    
