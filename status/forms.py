from django import forms
from .models import addStatus
from django.core.exceptions import ValidationError

class statusForms(forms.ModelForm):
    class Meta:
        model = addStatus
        fields = [
            'status_field',
        ]

        widgets = {
            'status_field': forms.TextInput(
                attrs = {
                    'class' : 'form-control', 'id' : 'status_field', 'placeholder' : "Enter your status here", 
                }
            )
        }
