import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import TestCase, Client
from django.urls import reverse, resolve
from login.views import login 
from django.contrib.auth import authenticate, login

# Create your tests here.

class LoginUnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.login = reverse('login')
        self.welcome = reverse('welcome')
        self.logout = reverse('logout')

    # Test Urls
    def test_login_urls_is_exist(self):
        url = self.client.get('/login')
        self.assertEquals(url.status_code, 200)

    # Redirect ke Login
    def test_welcome_not_authenticated(self):
        url = self.client.get('/welcome')
        self.assertEquals(url.status_code, 302) 

    def test_logout_is_redirect(self):
        url = self.client.get('/logout')
        self.assertEquals(url.status_code, 302)

    # Test Templates
    def test_login_render_templates(self):
        response = self.client.get(self.login)
        self.assertTemplateUsed(response, 'login/login.html')

    # Test Views
    def test_login_is_redirect(self):
        response = self.client.post(self.login)
        self.assertEquals(response.status_code, 302)

    def test_logged_in(self):
        login = self.client.post('/login', follow=True, data={
            'username': 'george',
            'password': 'georgeuser123',
        })
        self.assertTrue(login)

    def test_login_is_success(self):
        self.client.post('/login', follow=True, data={
            'username': 'george',
            'password': 'georgeuser123',
        })
        response = self.client.post('/welcome')
        self.assertEquals(response.status_code, 302)

    def test_login_is_unsuccess(self):
        self.client.post('/login', follow=True, data={
            'username': 'test',
            'password': 'testuser123',
        })
        response = self.client.post(self.login)
        self.assertEquals(response.status_code, 302)

    def test_logout_is_success(self):
        response = self.client.post(self.logout)
        self.assertEquals(response.status_code, 302)

class LoginFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(LoginFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(LoginFunctionalTest, self).tearDown()

    def test_login(self):
        selenium = self.selenium

        # Membuka link localhost yang ingin ditest
        selenium.get('http://127.0.0.1:8000/login')
        
        # Pilih elemen yang akan diberikan input username
        username_field = selenium.find_element_by_id('username')
        username_field.send_keys('george') 
        time.sleep(1)

        # Pilih elemen yang akan diberikan input password
        password_field = selenium.find_element_by_id('password')
        password_field.send_keys('georgeuser123') 
        time.sleep(1)

        # Klik tombol login
        login_button = selenium.find_element_by_class_name('submit')
        login_button.click()
        time.sleep(2)

        # Memastikan sudah redirect ke halaman welcome
        welcome =  selenium.find_element_by_class_name('judul-page-4').value_of_css_property('font-size')
        self.assertEquals(welcome, '71px')
        time.sleep(3)

        # Klik tombol logout
        logout_button = selenium.find_element_by_class_name('submit')
        logout_button.click()
        time.sleep(2)

        # Memastikan sudah redirect ke halaman login
        login = selenium.find_element_by_class_name('judul-page-3').value_of_css_property('font-size')
        self.assertEquals(login, '59px')
        time.sleep(3)



